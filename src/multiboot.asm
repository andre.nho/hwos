; Sets our kernel according to the multiboot specification.
; This allows our kernel to be booted by GRUB.
; (see specification: http://nongnu.askapache.com/grub/phcoder/multiboot.pdf)

section .multiboot_header
header_start:
	; multiboot 2 magic header
	dd	0xE85250D6
	; architecture 0 (protected mode i386)
	dd	0
	; header length
	dd	header_end - header_start  
	; checksum
	dd 	0x100000000 - (0xe85250d6 + 0 + (header_end - header_start))

	; TODO - optional mutliboot tags here

	; requred end tag
	dw	0	; type
	dw	0	; flags
	dw	8	; size
header_end:

; vim: syntax=nasm
