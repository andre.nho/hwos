global start

section .text
bits 32
start:
	mov	esp, stack_top		; create stack pointer

	call	check_multiboot
	call	check_long_mode

	; print green 'OK'
	mov	dword [0xB8000], 0x2F4B2F4F
	hlt

;
; check if the kernel was loaded via multiboot
;
check_multiboot:
	cmp 	eax, 0x36D76289
	jne	.no_multiboot
	ret
.no_multiboot:
	mov	al, "a"
	jmp	error

; 
; check for long mode (x64) support
;
check_long_mode:
	; test if extended processor info is available
	mov	eax, 0x80000000		; argument for cpuid
	cpuid				; get highest supported argument
	cmp	eax, 0x80000001		; if must be > 0x80000001
	jb	.no_long_mode

	; use extended info to test for long mode
	mov	eax, 0x80000001		; argument for extended processor info
	cpuid
	test	edx, 1<<29		; test if LM-bit is set in D register
	jz	.no_long_mode
	ret

.no_long_mode:
	mov 	al, "b"
	jmp	error

;
; prints 'ERR: ' and the given error code to screen, and hangs.
; pars:
;   - error code (in ascii) in AL
error:
	mov	dword [0xB8000], 0x4F524F45
	mov	dword [0xB8004], 0x4F3A4F52
	mov	dword [0xB8008], 0x4F204F20
	mov	byte  [0xB800A], al
	hlt

section .bss

;
; pages
;
align 4096
p4_table:
	resb 4096
p3_table:
	resb 4096
p2_table:
	resb 4096

;
; stack
;
stack_bottom:
	resb	64
stack_top:

; vim: syntax=nasm
