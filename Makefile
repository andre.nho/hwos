OBJ=src/multiboot.o src/boot.o

kernel.elf: $(OBJ)
	ld -n -o $@ -T linker.ld $^

%.o: %.asm
	nasm $^ -f elf64 -F dwarf -g -o $@

hwos.iso: grub.cfg kernel.elf
	mkdir -p isofiles/boot/grub
	cp grub.cfg isofiles/boot/grub/
	cp kernel.elf isofiles/boot/
	grub-mkrescue -o $@ isofiles
	rm -rf isofiles

test: hwos.iso
	qemu-system-x86_64 -cdrom $^

debug: hwos.iso
	qemu-system-x86_64 -s -S -cdrom $^ &
	sleep 0.1
	gdb -ex 'target remote localhost:1234' -s kernel.elf 

clean:
	rm -f $(OBJ) kernel.elf hwos.iso
	rm -rf isofiles

.PHONY: clean test
